from .xml_style import XMLDataset
from .registry import DATASETS

@DATASETS.register_module
class MyDataset(XMLDataset):

    CLASSES = ( "Iron", )